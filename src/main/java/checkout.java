package main.java;
import java.util.HashMap;
import com.africastalking.PaymentService;
import com.africastalking.AfricasTalking;
import com.africastalking.payment.response.CheckoutResponse;
public class checkout
{
    static String username = "sandbox";
    static String apiKey   = "115b5bd9e3d0a91cdf5626fe81216fbfcdb0d95ca1c15aeb5f44568ad977ea54";
    String productName = "My Online Store";
    String phoneNumber;
    String currencyCode = "KES";
    Float price;
    CheckoutResponse paid;
    public void setApiKey(String apiKeyMe)
    {
        apiKey = apiKeyMe;
    }
    public void setPhoneNumber(String phonenumber)
    {
        phoneNumber = phonenumber;
    }
    
    /**
     * called once the amount of a checkout has been received.
     */
    public void setAmount(Float price) {
        this.price = price;
    }
    /**
     * a call to this method means initPayment() works well / a payment has been initiated.
     * During testing using Mockito, this method is used to assert whether initpayment() works. If initpayment() is called
     * by a mock mqttClient and perpetually a call made to forTest, then initpayment() works well
     */
    public String forTest(CheckoutResponse cr)
    {   
    System.out.println(cr);
    return "response printed";
    }
    public CheckoutResponse initPayment()
    {
        AfricasTalking.initialize(username,apiKey);
        PaymentService mPay = AfricasTalking.getService(AfricasTalking.SERVICE_PAYMENT);
        HashMap<String, String> metadata = new HashMap<>();
        metadata.put("agentId", "654");
        metadata.put("productId", "321");
     
        try
        {
            paid = mPay.mobileCheckout(productName, phoneNumber, currencyCode, price,metadata);
            forTest(paid);
            return paid;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            System.out.println("Received error response: " + ex.getMessage());
        }
    return paid;
    }
}