/**
 * receives 
 */
package main.java;
import com.google.gson.JsonObject;
import spark.Request;
import spark.Response;
import com.google.gson.Gson;

public class CallbackController {
    public static JsonObject json = new JsonObject();
    public CallbackController(){
    }
    public Object handleMpesa(Request req, Response res){

        return doMpesa(req, res);
    }
    private Object doMpesa(Request req, Response res){
/**
 * the req.body is the callback and it is a json object
 * it is changed to an Mpesa object using GSON
 */
       System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Here is the confirmation:  ");
        System.out.println(req.body());
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        Mpesa received = new Gson().fromJson(req.body(),Mpesa.class);
        System.out.println("-------------------------------------------------");
        System.out.println("Payment Status: "+ received.getStatus());
        System.out.println("-------------------------------------------------");
        publisher send_0k = new publisher();
        send_0k.setContent(received.getStatus());
        send_0k.publish();
        mpesaObjectGotten();
        return received;

    }
    /**
     * Used during testing.
     * When a mock CallbackController calls doMpesa and mpesaObjectGotten() is perpetually called it means doMpesa works ok.
     */
    public String mpesaObjectGotten(){
    return "Mpesa done";
    
    }
}
