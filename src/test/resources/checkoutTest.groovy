package test.resources.checkoutTest

import com.africastalking.payment.response.CheckoutResponse
import main.java.checkout
import org.junit.*
import static groovy.test.GroovyAssert.*
import static org.mockito.Mockito.*

class checkoutTest {
    // def trial = new checkout();
    @BeforeClass
    static void setUpBeforeClass() {
        println "Inside setUpBeforeClass()"
    }

    @Before
    void setUp() {
        println "Inside setUp()"
    }

    @Test
    void testSetPhoneNumber() {
       def trial = new checkout();
        String testPN = "+254791084323"
        trial.setPhoneNumber(testPN)
        boolean correctNumber = (trial.phoneNumber == testPN)
        assertTrue(correctNumber)
    }
    @Test
    void testSetPrice() {
        def trial = new checkout();
        Float testPrice = 10.5f
        trial.setPrice(testPrice)
        boolean correctPrice = (trial.price == testPrice)
        assertTrue(correctPrice)
    }
    @Test
    void testInitPayment() {
        def trial = mock(checkout.class)
        when(trial.forTest()).thenReturn("initpay() works")
        trial.initPayment()
        assertEquals(trial.forTest(), "initpay() works")

    }
    @After
    void tearDown() {
        println "Inside tearDown()"
    }
 
    @AfterClass
    static void tearDownAfterClass() {
        println "Inside tearDownAfterClass()"
    }
}
