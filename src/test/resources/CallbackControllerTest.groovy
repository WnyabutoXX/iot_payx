package test.resources
import main.java.CallbackController;
import spark.Request;
import spark.Response;
import org.junit.Before
import org.junit.BeforeClass
import org.junit.jupiter.api.Test
import static org.mockito.Mockito.*

class CallbackControllerTest extends GroovyTestCase {

    @BeforeClass
    static void setUpBeforeClass() {
        println "Inside setUpBeforeClass()"
    }

    @Before
    void setUp() {
        println "Inside setUp()"
    }
    @Test
    void testHandleMpesa() {

        def callbackController =  mock(CallbackController.class)
        def req = mock(Request.class)
        def res = mock (Response.class)
        when(callbackController.mpesaObjectGotten()).thenReturn("handleMpesa() works")
        callbackController.handleMpesa(req, res)
        assertEquals(callbackController.mpesaObjectGotten(),"handleMpesa() works")

    }
}
