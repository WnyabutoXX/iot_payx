package test.resources
import main.java.publisher
import static org.mockito.Mockito.*
import org.junit.*
class publisherTest extends GroovyTestCase {
    @BeforeClass
    static void setUpBeforeClass() {
        println "Inside setUpBeforeClass()"
    }

    @Before
    void setUp() {
        println "Inside setUp()"
    }
    @Test
    void testSetContent() {
       def pubLisher = new publisher()
        pubLisher.setContent("success")
        GroovyTestCase.assertEquals("success", pubLisher.content)
    }
    @Test
    void testPublish() {
        def pubL = mock(publisher.class)
        when(pubL.pubSuccess()).thenReturn("Publish() works")
        pubL.publish()
        assertEquals(pubL.pubSuccess(),"Publish() works")

    }
    @After
    void tearDown() {
        println "Inside tearDown()"
    }

    @AfterClass
    static void tearDownAfterClass() {
        println "Inside tearDownAfterClass()"
    }

}
