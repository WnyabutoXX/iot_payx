
package test.resources.mqttClientTest
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttMessage
import static org.mockito.Mockito.*
import static groovy.test.GroovyAssert.*
import org.junit.*
import main.java.mqttClient
class mqttClientTest extends GroovyTestCase {
    @BeforeClass
    static void setUpBeforeClass() {
        println "Inside setUpBeforeClass()"
    }

    @Before
    void setUp() {
        println "Inside setUp()"
    }
    @Test
    void testRunClient(){
        def connTest = mock(mqttClient.class)
        when(connTest.makeConnection()).thenReturn("runClient() works")
        connTest.runClient()
        assertEquals(connTest.makeConnection(), "runClient() works")

    }

    @After
    void tearDown() {
        println "Inside tearDown()"
    }

    @AfterClass
    static void tearDownAfterClass() {
        println "Inside tearDownAfterClass()"
    }
}
